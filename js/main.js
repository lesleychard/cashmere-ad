$(document).on('click touchstart', 'video, #video-play', function(e) {
	var video = $('video').get(0);
	$('#video-play').toggleClass('hide');

	if(video.paused) {
		video.play();
	} else {
		video.pause();
	}
});

$(document).ready(function(){
	var designNum = getRandom(1,15);
	$('#img-modified').css('background-image', 'url(img/modified/design' + designNum + '.jpg)');
	$('#img-original').css('background-image', 'url(img/original/design' + designNum + '.jpg)');
});

function getRandom(min, max) {
    return min + Math.floor(Math.random() * (max - min + 1));
}